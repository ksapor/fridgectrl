from TempCycles import Section, TempCycle
from Oven import Oven
import datetime

#== Stale ======================================================================
PORTNAME = 'COM4'
LOGNAME = "fridge_log_%s.txt"%str(datetime.date.today())
#LOGNAME = 'setup_nosignal_21h_%s.txt'%str(datetime.date.today())
#===============================================================================

#== Parametry ==================================================================
TEMP_MAX = 50.0
TEMP_MIN = 15.0
TEMP_STEP = 0.5

TIME_FLAT   = 2*60      # w MINUTACH
TIME_SLOPE  = 4*60      # w MINUTACH
CYCLE_NUM   = 3         # liczba PELNYCH cykli temperaturowych (obiekt cycle)
#===============================================================================

#== Definicje sekcji ===========================================================
s1 = Section(time=TIME_FLAT,    start_temp=TEMP_MIN, stop_temp=TEMP_MIN)
s2 = Section(time=TIME_SLOPE,   start_temp=TEMP_MIN, stop_temp=TEMP_MAX, step=TEMP_STEP)
s3 = Section(time=TIME_FLAT,    start_temp=TEMP_MAX, stop_temp=TEMP_MAX)
s4 = Section(time=TIME_SLOPE,   start_temp=TEMP_MAX, stop_temp=TEMP_MIN, step=TEMP_STEP)

cycle = TempCycle(s1, s2, s3, s4)
#===============================================================================

#== Obiekt - lodowka ===========================================================
oven = Oven(PORTNAME)
#===============================================================================

#== Profil temperaturowy =======================================================
try:
    oven.startLog(LOGNAME)              # plik z logiem lodowki (pomaga sledzic ew. bledy)
    for i in range(CYCLE_NUM):          # petla po cyklach temperaturowych
        cycle.executeCycle(oven)        # wykonanie pojedynczego cyklu
    s1.execute(oven)                    # domkniecie ostatniego cyklu plaska sekcja
        
except KeyboardInterrupt:
    message = 'Exiting loop'
    print(message)
    oven.writeToLog(message + '\n')
 
except Exception as exc:
    message = 'Failure'
    print(message)
    print(exc)
    oven.writeToLog( message + '\n' + str(exc) + '\n')

oven.closeCOM()                         # zamkniecie komunikacji z lodowka
oven.stopLog()                          # zamkniecie pliku z logiem lodowki
#===============================================================================