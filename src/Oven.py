import serial
import datetime, time

#===============================================================================
class Oven():
    '''
    This class implements communication with Oven-class climate chamber :)
    To initialize an object, one needs to give serial port name as an argument,
    e.g.
        oven = Oven('COM4')
    Then, the communication with climate chamber should be established.
    
    To set the desired temperature setpoint, use setTemp(t) method.
    Example:
        oven.setTemp(20.0)
    
    This class also allows the user to create a logfile, which will contain
    records from communication with the climate chamber.
    Example:
        oven.startLog('fridge_log.txt')    # opens logfile named 'fridge_log.txt'
        
    Every usage of setTemp() method leaves a record in the logfile. Apart from that
    the user can save additional information in logfile using writeToLog(s) method.
    Example:
        message = 'BLABLA'
        oven.writeToLog(message + '\n')
    
    When the program ends, both communication and logfile should be closed by the user.
    This is accomplished with methods: closeCOM() and stopLog().
    Example:
        ...    # program ends
        oven.closeCOM()
        oven.stopLog()
    '''
    #===========================================================================
    def __init__(self, portname):
        self.ser = serial.Serial(port=portname, baudrate=9600, bytesize=serial.EIGHTBITS, 
              parity=serial.PARITY_NONE, stopbits=serial.STOPBITS_ONE, 
              timeout=1, xonxoff=False, rtscts=False)
        
        self.ser.close()
        self.ser.open()
        if(self.ser.isOpen()):
            print('Port %s opened.'%self.ser.name)
        else:
            raise serial.SerialException("Error occured while opening serial port.")
        
        self.ser.flushInput()
        self.ser.flushOutput()
        print('Established connection to oven.')
        
        self.logflag = 0
        self.error_count = 0
        self.total_error = 0
    #===========================================================================
    
    #===========================================================================
    def startLog(self, logname):
        '''
        This method allows the user to create a logfile, which will contain
        records from communication with the climate chamber.
        Example:
            oven.startLog('fridge_log.txt')    # opens logfile named 'fridge_log.txt'
        '''
        try:
            self.logflag = 1
            self.logfile = open(logname, 'w')
            self.logfile.write('Oven control log.\n' + str(datetime.datetime.now()) + '\n')
            print('Log file %s successfully opened.'%logname)
        except:
            self.logflag = 0
            print('Opening log file has failed.')
    #===========================================================================
    
    #===========================================================================
    def writeToLog(self, s):
        '''
        The user can save additional information in logfile using writeToLog(s) method.
        Example:
            message = 'BLABLA'
            oven.writeToLog(message + '\n')    # this saves 'BLABLA\n' string to logfile
        '''
        if(self.logflag == 1):
            self.logfile.write(s)
    #===========================================================================
    
    #===========================================================================
    def stopLog(self):
        '''
        Closes logfile opened with the startLog() method.
        Example:
            oven.stopLog()
        '''
        if(self.logflag == 1):
            self.logfile.write('Total error count: %d\n'%self.total_error)
            self.logfile.write( str(datetime.datetime.now()) + '\n' )
            self.logfile.write('End of file.\n')
            self.logfile.close()
    #===========================================================================
    
    #===========================================================================
    def setTemp(self, temperature):
        '''
        Sets the desired temperature setpoint [in CELSIUS DEGREES].
        Example:
            oven.setTemp(20.0)    # sets the temperature inside climate chamber to 20.0 C
        '''
        self.ser.write(b'T %.1f\r'%temperature)
        time.sleep(0.5)
        
        line = [b'blabla', b'blabla']
        for i in range(2):
            line[i] = self.ser.readline()
            print( line[i].decode('utf-8') )
        
        if( (line[0] == b'T %.1f\r\n'%temperature) ):    
            self.writeToLog( line[0].decode('utf-8') )
            self.writeToLog( line[1].decode('utf-8') )
        else:
            self.ser.write(b'\r')
            time.sleep(0.5)
            self.ser.flushInput()
            self.ser.flushOutput()
            if(self.error_count < 5):
                self.error_count = self.error_count + 1
                self.setTemp(temperature)
            else:
                self.writeToLog('Comm error!\n')
                print('Comm error!')
                self.error_count = 0
                self.total_error = self.total_error + 1
    #===========================================================================
    
    #===========================================================================
    def closeCOM(self):
        '''
        Closes serial port used for communication with the climate chamber.
        Use this method at the end of the program.
        Example:
            oven.closeCOM()
        '''
        self.ser.flushInput()
        self.ser.flushOutput()
        self.ser.close()
        print('Port %s closed.'%self.ser.name)
    #===========================================================================
    
#===============================================================================
