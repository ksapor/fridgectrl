import time
import numpy as np
from enum import Enum

#===============================================================================
class Edge(Enum):
    Rising = 'Rising'
    Falling = 'Falling'
    Flat = 'Flat'
#===============================================================================

#===============================================================================
class Section():
    '''
    This class implements a single temperature section which can be a part of a bigger cycle.
    To define a section, one needs to specify it's duration (time argument) [in MINUTES],
    starting and ending temperatures [in CELSIUS DEGREES], and temperature step [in CELSIUS DEGREES].
    Time argument needs to be an integer number.
    
    Example:
    #===========================================================================
    # This creates a section that will start at temperature of 20 C,
    # end at temperature of 50 C, last for 60 minutes (1 hour),
    # and change temperature with 0.5 C step.
        s1 = Section(time = 60, start_temp = 20.0, stop_temp = 50.0, step = 0.5)
    #===========================================================================
    '''
    #===========================================================================
    def __init__(self, time, start_temp, stop_temp, step = None):
        #== Assertions =========================================================
        if( start_temp < 10.0 ) or ( start_temp > 50.0 ) or ( stop_temp < 10.0 ) or ( stop_temp > 50.0 ):
            raise ValueError('Starting and ending temperatures have to be in [10.0, 50.0] interval.')
        
        t1_int = np.int(start_temp)
        t2_int = np.int(stop_temp)
        if ( np.int((start_temp - t1_int)*10) != 5 ) and ( np.int((start_temp - t1_int)*10) != 0 ):
            raise ValueError('Starting temperature can be specified only with 0.5 C accuracy.')
        if ( np.int((stop_temp - t2_int)*10) != 5 ) and ( np.int((stop_temp - t2_int)*10) != 0 ):
            raise ValueError('Ending temperature can be specified only with 0.5 C accuracy.')
        #=======================================================================
        
        #== Basic params =======================================================
        self.start = np.float( '%.1f'%start_temp )
        self.stop = np.float( '%.1f'%stop_temp )
        self.time = np.int(time)            # in MINUTES
        #=======================================================================

        #== Calculate total temp change and edge type ==========================
        self.total = self.stop - self.start
        if(self.total > 0):
            self.edge = Edge.Rising
        elif(self.total < 0):
            self.edge = Edge.Falling
        else:
            self.edge = Edge.Flat
        #=======================================================================
        
        #== Check changing rate and step =======================================
        if (self.total / self.time) > 0.5:
            raise ValueError('Temperature change rate should be less than 0.5 K/min.')
        if (self.edge != Edge.Flat):
            if(step == None):
                raise ValueError('Step argument must be specified for non-flat sections!')
            else:
                step_int = np.int(step)
                if ( np.int((step - step_int)*10) != 5 ) and ( np.int((step - step_int)*10) != 0 ):
                    raise ValueError('Step argument can be specified only with 0.5 C accuracy.')
                
                self.step = np.float( '%.1f'%step )
        #=======================================================================
        
        #== Calculating time interval ==========================================
        self.time = self.time*60                    # conversion to SECONDS
        if( self.edge != Edge.Flat):
            self.Npoint = np.abs( self.total / self.step )
            self.timeInt = (self.time / self.Npoint)    # in SECONDS
        #=======================================================================
    #===========================================================================

    #===========================================================================
    def getStartTemp(self):
        return self.start
    
    def getStopTemp(self):
        return self.stop
    
    def getEdgeType(self):
        return self.edge.value
    #===========================================================================

    #===========================================================================
    def execute(self, oven):
        '''
        Method used to execute defined temperature section. To work properly, it needs
        an Oven class object passed as an argument, which should have setTemp(t) method
        implemented. This method uses setTemp(t) method to set desired temperature setpoints
        in the target climate chamber.
        '''
        temperature = np.float(self.start)
        if(self.edge == Edge.Rising):
            step = self.step
            sleep_time = self.timeInt
        elif(self.edge == Edge.Falling):
            step = -self.step
            sleep_time = self.timeInt
        else:
            step = 0
            sleep_time = self.time
            
        limit = self.stop - step
        while(1):
            temperature = temperature + step
            oven.setTemp(temperature)
            time.sleep(sleep_time)
            
            if( self.edge == Edge.Rising ):
                if(temperature >= limit):
                    break
            elif( self.edge == Edge.Falling ):
                if(temperature <= limit ):
                    break
            else:
                break
    #===========================================================================
#===============================================================================

#===============================================================================
class TempCycle():
    '''
    This class implements a whole temperature cycle. It can consist of any number
    of sections. The sections must be defined with objects of Section class.
    
    Example:
        s1 = Section(...)
        s2 = Section(...)
        .
        .
        .
        sn = Section(...)
        cycle = TempCycle(s1, s2, ..., sn)
        cycle.executeCycle(oven)
        
    The oven object must implement a setTemp(t) method, which sets the temperature
    of the target climate chamber to a value specified by argument t.
    '''
    #===========================================================================
    def __init__(self, *arg_list):
        self.sections = []
        for arg in arg_list:
            if( type(arg) is not Section ):
                raise TypeError('Every argument of arg_list must be of type Section.')
            else:
                self.sections.append(arg)
        
        for i in range( len(self.sections) )[:-1]:
            t1 = np.int( self.sections[i].getStopTemp()*10 )
            t2 = np.int( self.sections[i+1].getStartTemp()*10 )
            if( t1 != t2 ):
                print('WARNING: Section %d ends at a different temperature than section %d begins.'%(i+1, i+2) )            
    #===========================================================================
    
    #===========================================================================
    def executeCycle(self, oven):
        '''
        Method used to execute defined temperature cycle. To work properly, it needs
        an Oven class object passed as an argument, which should have setTemp(t) method
        implemented. This method uses setTemp(t) method to set desired temperature setpoints
        in the target climate chamber.
        
        Oven class object should also have writeToLog(s) method defined, which allows to print messages into the logfile. 
        If this functionality is not needed, the method writeToLog(s) can be an empty method definition in the Oven class. 
        It has to be defined in Oven class interface for this method to work properly.
        '''
        border = '#========================='
        for sec in self.sections:
            print(border)
            oven.writeToLog(border + '\n')
            
            msg = '%s section'%sec.getEdgeType()
            print(msg)
            oven.writeToLog(msg + '\n')
            
            sec.execute(oven)
            
            print(border)
            oven.writeToLog(border + '\n')
    #===========================================================================
#===============================================================================